<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePartiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parties', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('party_name');
            $table->string('party_image')->default('default.png');
            $table->string('party_description');
            $table->datetime('start_date');
            $table->datetime('end_date');
            $table->string('party_address');
            $table->boolean('is_private');
            $table->boolean('is_guest_invite');
            $table->boolean('is_bring_booze');
            $table->integer('guest_invite_count');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parties');
    }
}
