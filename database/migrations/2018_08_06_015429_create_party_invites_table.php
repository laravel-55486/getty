<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePartyInvitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('party_invites', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('party_id');
            $table->integer('guest_id');
            $table->integer('status')->default(0);//0 default is "sent", 1 "accepted", 2 "rejected"
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('party_invites');
    }
}
