<?php

namespace App\Http\Controllers\Api;

use App\User;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public function getAllusers()
    {
        $users = User::all();

        return response()->json(['result' => 'success', 'data' => $users]);
    }

    public function login()
    {
        if(Auth::attempt(['username' => request('username'), 'password' => request('password')])){
            $user = Auth::user();
            $success['token'] =  $user->createToken('MyApp')-> accessToken;
            $success['user'] = $user;
            return response()->json(['result' => 'success','data' => $success], 200);
        } else{
            return response()->json(['result'=>'error', 'msg' => 'Invalid username or password.'], 401);
        }
    }

    public function usernameCheck(Request $request)
    {
        $username = $request->username;
        $usernameCount = User::where('username', $username)->count();
        if ($usernameCount > 0) {
            return response()->json(['result'=>'exist'], 401);
        } else {
            return response()->json(['result'=>'new'], 401);
        }
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'full_name' => 'required',
            'username' => 'required|unique:users',
            'birthday' => 'required',
            'email' => 'email|unique:users',
            'phone_number' => 'unique:users',
            'password' => 'required',
            'c_password' => 'required|same:password',
        ]);
        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }
        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $user = User::create($input);
        $success['token'] =  $user->createToken('MyApp')-> accessToken;
        $success['username'] =  $user->username;
        return response()->json(['success'=>$success], 200);
    }

    public function details()
    {
        $user = Auth::user();
        return response()->json(['result' => 'success','user' => $user], 200);
    }
}
