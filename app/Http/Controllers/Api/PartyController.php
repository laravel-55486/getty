<?php

namespace App\Http\Controllers\Api;

use File;
use Auth;
use DateTime;
use App\User;
use App\Party;
use App\PartyInvite;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PartyController extends Controller
{
    protected function encode_date_format($date)
    {
        $selectedDate = DateTime::createFromFormat('m/d/Y H:i', $date);
        $finalDate = $selectedDate->format('Y-m-d H:i:s');
        return $finalDate;
    }

    public function createParty(Request $request)
    {
        $user = Auth::user();
        $new_party = new Party;
        $new_party->user_id = $user->id;
        $new_party->party_name = $request->party_name;
        $new_party->party_description = $request->party_description;
        $new_party->start_date = $this->encode_date_format($request->start_date);
        $new_party->end_date = $this->encode_date_format($request->end_date);
        $new_party->party_address = $request->party_address;
        $new_party->is_private = $request->is_private;
        $new_party->is_guest_invite = $request->is_guest_invite;
        $new_party->is_bring_booze = $request->is_bring_booze;
        $new_party->guest_invite_count = $request->guest_invite_count;
        $new_party->save();

        if ($request->image) {
            $data = $request->image;

            $imageRand = time();
            $random_name = $new_party->id."_".$imageRand.".png";

            if(!is_dir(public_path('uploads/parties/'.$new_party->user_id))){
                mkdir(public_path('uploads/parties/'.$new_party->user_id));
            }

            $dst = public_path('uploads/parties/'.$new_party->user_id.'/');

            if($new_party->party_image != 'default.png') {
                if (File::exists($dst . $new_party->party_image)) {
                    File::delete($dst . $new_party->party_image);
                }
            }

            $path = $dst.'/'.$random_name;

            $data = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $data));

            file_put_contents($path, $data);

            $new_party->party_image = $random_name;
            $new_party->save();

            $new_party['image_url'] = asset('uploads/parties/'.$new_party->user_id.'/'.$new_party->party_image);
        } else {
            $new_party['image_url'] = asset('uploads/parties/default.png');
        }

        return response()->json(['result'=>'success', 'party' => $new_party,], 200);
    }

    public function sendPartyInvite(Request $request)
    {
        $user_ids = $request->user_ids;
        foreach ($user_ids as $user_id) {
            $invite_count = PartyInvite::where('party_id', $request->party_id)->where('guest_id', $user_id)->count();
            if ($invite_count == 0) {
                $new_partyInvite = new PartyInvite;
                $new_partyInvite->party_id = $request->party_id;
                $new_partyInvite->guest_id = $user_id;
                $new_partyInvite->save();
            }
        }

        return response()->json(['result' => 'success'], 200);
    }

    public function acceptPartyInvite(Request $request)
    {
        $user = Auth::user();
        $party_id = $request->party_id;
        $my_invite = PartyInvite::where('party_id', $party_id)->where('guest_id', $user->id)->first();
        if ($my_invite) {
            $my_invite->status = 1;
            $my_invite->save();

            return response()->json(['result' => 'success']);
        }
        return response()->json(['result' => 'error', 'msg' => 'can not find correct invite']);
    }

    public function rejectPartyInvite(Request $request)
    {
        $user = Auth::user();
        $party_id = $request->party_id;
        $my_invite = PartyInvite::where('party_id', $party_id)->where('guest_id', $user->id)->first();
        if ($my_invite) {
            $my_invite->status = 2;
            $my_invite->save();

            return response()->json(['result' => 'success']);
        }
        return response()->json(['result' => 'error', 'msg' => 'can not find correct invite']);
    }

    public function getAllParties()
    {
        $parties = Party::orderBy('start_date', 'ASC')->get();
        $final_parties = array();
        foreach ($parties as $party) {
            $party_owner = User::find($party->user_id);
            $party_guests = PartyInvite::where('party_invites.party_id', $party->id)->where('party_invites.status', 1)
            ->join('users', 'users.id', '=', 'party_invites.guest_id')
            ->select('users.*')->get();

            $party['owner'] = $party_owner;
            $party['guests'] = $party_guests;

            $final_parties[] = $party;
        }

        return response()->json(['result' => 'success', 'data' => $final_parties]);
    }

    public function getSpecificParty($user_id)
    {
        $party = Party::where('user_id', $user_id)->orderBy('start_date', 'ASC')->get();

        return response()->json(['result' => 'success', 'data' => $party]);
    }
}
