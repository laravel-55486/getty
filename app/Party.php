<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Party extends Model
{
    protected $fillable = [
        'user_id',
        'party_name',
        'party_description',
        'start_date',
        'end_date',
        'party_address',
        'is_private',
        'is_guest_invite',
        'is_bring_booze',
        'guest_invite_count',
        'party_image',
    ];
}
