<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PartyInvite extends Model
{
    protected $fillable = [
        'party_id',
        'guest_id',
        'status',
    ];
}
