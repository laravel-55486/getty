<?php

use Illuminate\Http\Request;

Route::get('sendautherror', function() {
    return response()->json(['result' => 'error', 'msg' => 'UnAuthenticated']);
});

Route::post('login', 'Api\UserController@login');
Route::post('username_check', 'Api\UserController@usernameCheck');
Route::post('register', 'Api\UserController@register');

Route::get('allUsers', 'Api\UserController@getAllusers');
Route::get('allParties', 'Api\PartyController@getAllParties');
Route::get('getParty/{userId}', 'Api\PartyController@getSpecificParty');

Route::group(['middleware' => 'auth:api'], function(){
    Route::get('detail', 'Api\UserController@details');

    Route::prefix('party')->group(function () {
        Route::post('/post', 'Api\PartyController@createParty');
        Route::post('/invite', 'Api\PartyController@sendPartyInvite');
        Route::post('/invite/accept', 'Api\PartyController@acceptPartyInvite');
        Route::post('/invite/reject', 'Api\PartyController@rejectPartyInvite');
    });
});
